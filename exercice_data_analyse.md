# Exercice - data analyse

## Introduction

Lors de ce test vous allez manipuler deux tables de données du Système National des Données de Santé (SNDS). Ces données, fictives, ne sont pas de vrais parcours de soins, ne vous formalisez donc pas en cas d'incohérences notamment sur les dates. L'objectif est de vous faire réaliser des étapes simples de data management et visualisation.

Le test est à réaliser en une heure approximativement. Vous trouverez les deux tables `T_MCOaaB` et `T_MCOaaC` dans le dossier data1.

Pour en savoir plus sur le SNDS et comment l'utiliser, nous vous invitons à vous référer au [dictionnaire interactif du SNDS](https://health-data-hub.shinyapps.io/dico-snds/) et à la [documentation collaborative](https://documentation-snds.health-data-hub.fr/).   

## Contexte

Le SNDS historique est une base de données médico-administrative comportant principalement les remboursements de soins de ville et les hospitalisations pour la quasi-totalité de la population française. De par son système de sécurité sociale, la France fait partie des rares pays qui disposent de systèmes d’informations médico-administratives couvrant l’ensemble du territoire et de la population.
Aujourd’hui le SNDS, c’est plus de 180 tables, plus de 4500 variables, de nombreuses règles métiers, une évolution constante, 1.2 milliards de feuilles de soin par an, 11 millions de séjour hospitalier par an. Cela en fait une des plus grosses bases administratives au monde : 450 To de données au total.

Le SNDS est alimenté par trois bases de données :
* le SNIIRAM (Système National d’Information Inter-Régimes de l’Assurance Maladie) : Les Caisses Primaires d’Assurance Maladie remontent l’ensemble des informations issues des remboursements à la CNAM (Caisse Nationale de l’Assurence Maladie)
* le PMSI (Programme de Médicalisation des Systèmes d’Information) : Chaque établissement de santé enregistre chacun des séjours hospitaliers sous forme derésumés de sortie standardisé (RSS) qui sont ensuite transmis à l’Agence Technique de l’Information Hospitalière (ATIH). Ce dernier remonte ensuite les données consolidées à la CNAM pour intégration dans le SNDS. 
* le CépiDc : Le CépiDc de l’Inserm gère la Base de Causes Médicales de Décès (BCMD). Il ne traite que la partie médicale du certificat de décès. Par conséquent, la base ne contient aucun nom. Les données sont transmises à la CNAM pour intégration dans le SNDS. 

## Questions

1. Ouvrir les deux tables, et enlever les doublons et les valeurs manquantes. 
2. Quelle est la distribution du nombre de séjours hospitaliers par patient ? Vous prendrez garde à effectuer les bonnes jointures entre les tables. 
3. Représentez la durée de séjour moyenne en fonction de l'âge des patients au moment du séjour. Vous calculerez ces durées à partir de `EXE_SOI_DTD` et `EXE_SOI_DTF`
4. Quel(s) outil(s) utiliseriez-vous pour faire ce même exercice si les tables ne tenaient pas dans votre RAM ? Il n'est pas demandé de refaire l'exercice dans un contexte de grande volumétrie mais seulement de donner des pistes de solution. 
