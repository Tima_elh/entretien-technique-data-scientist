# Exercice - Machine Learning

## Introduction

Cet exercice est très simple puisqu'il s'agit d'une tâche relativement classique de classification de patients ayant ou non connu un Accident Vasculaire Cérébral (AVC).

Vous trouverez les données dans le dossier `data_ML`. 

## Objectifs

L'objectif est de construire un modèle de prédiction d'AVC (variable `stroke`) en utilisant le reste des variables explicatives. **Vous ne serez pas noté sur les performances du modèle**, il ne s'agit pas d'une compétition Kaggle entre participants, nous cherchons ici à comprendre vos réflèxes et démarche générale de Data Scientist face à un intitulé classique. Plusieurs grandes catégories d'étapes sont toutefois attendues :
- Analyse exploratoire des données (EDA)
- Si jugé nécessaire, nettoyage des données et feature engineering
- Entraînement de quelques modèles simples (maximum 3 modèles)
- Evaluation et comparaison des performances

Enfin, si vous disposiez de plus de temps, que feriez-vous pour améliorer les performances du modèle ? Question ouverte n'appelant pas à du code, vous pouvez rédiger la réponse succinctement dans le Jupyter Notebook directement.
